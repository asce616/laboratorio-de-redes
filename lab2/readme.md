![# Welcome to StackEdit!](http://pagina.fciencias.unam.mx/sites/default/files/logoFC_2019.png)  
# Taller de Sistemas Operativos, Redes de Cómputo, Sistemas Distribuidos y Manejo de Información



## Practica 2: Servidor web con AWS EC2 

## Objetivo:

El alumno aprenderá a crear un servidor web (Apache 2) en la nube de AWS del tipo IaaS (EC2) , además preparará la infraestructura necesaria para un flujo de trabajo en la nube tipo C.D ( Entrega continua ) elemento fundamental de la metodología DevOps.

Finalmente el alumno comprenderá de manera práctica los conceptos de nube, IaaS (Infraestructura como servicio), IP Pública, puertos, politicas de seguridad

## Introducción

El protocolo HTTPS (HyperText Transfer Protocol Secure) es un protocolo de la Capa de aplicación que permite el envió de información cifrada usando los protocolos HTTP y SSL/TLS. El protocolo HTTP envía información en claro a través del medio, el protocolo SSL/TLS es el encargado de encapsular el protocolo HTTP para ser enviado de manera cifrada.

### Cómputo en la nube

#### IaaS

#### AWS EC2 

## Prerequisitos
#### 1. Contar con usuario y contraseña de la cuenta AWS de Ciencias, en caso de no contar con ello no se podrá realizar la práctica, se revisaran los casos de alumnos especiales directamente conmigo 
#### 2. Crear un usuario con el perfil de Administrador, siguiendo el siguiente video: [Crear usuario Administrador](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab2/videos/lab2-crear-usuario.mp4?expanded=true&viewer=rich)

## Desarrollo



2. 

## Evaluación

3. 

### Notas adicionales
1. El reporte se entrega de manera individual.
3. Incluir las respuestas del Cuestionario en el reporte.
4. Se pueden agregar posibles errores, complicaciones, opiniones, críticas de la práctica o del laboratorio, o cualquier comentario relativo a la práctica.
5. Subir los archivos relacionados con la práctica a Moodle o entregar vía PDF al correo ismael.andrade@ciencias.unam.mx con el título del correo: [lab1-**nombre**-**apellido**]

### Errores comunes

